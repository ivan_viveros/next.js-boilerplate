import { DefaultSeoProps } from "next-seo";

const defaultSeoProps: DefaultSeoProps = {
  titleTemplate: "%s | Yaydoo",
  additionalLinkTags: [
    {
      rel: "icon",
      type: "image/svg+xml",
      href: "/favicon.svg",
    },
    {
      rel: "mask-icon",
      color: "#000000",
      href: "/favicon.svg",
    },
  ],
};

export default defaultSeoProps;
