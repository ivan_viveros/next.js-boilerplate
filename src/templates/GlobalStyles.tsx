import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
  body {
    font-family: "Poppins", sans-serif;
  }
`;

export default GlobalStyles;
