import type { GetServerSideProps, NextPage } from "next";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { NextSeo } from "next-seo";
import Link from "next/link";
import { useRouter } from "next/router";

export const getServerSideProps: GetServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale!, ["common", "home"])),
    },
  };
};

const Home: NextPage = () => {
  const { t } = useTranslation(["common", "home"]);

  const router = useRouter();

  return (
    <div>
      <NextSeo title={t("home:title")} />
      <h1>{t("home:headline")}</h1>
      <p>{t("home:description")}</p>
      <Link passHref href="/" locale={router.locale === "es" ? "en" : "es"}>
        <a>{t("common:switch-language")}</a>
      </Link>
    </div>
  );
};

export default Home;
